# Big Project

This project is my first homework assignment for the SKJ course. It includes a simple Python script that performs various tasks.

## Installation

1. Clone this repository using the command: `git clone https://gitlab.com/puk0023/big-project.git`

2. Install the required packages using the command: `pip install -r requirements.txt`

## Usage

1. Run the script using the command: `python tasks.py`

2. Follow the prompts displayed in the terminal.

## Contributions

Contributions to this project are welcome! If you find a bug or have an idea for a new feature, feel free to open an issue or submit a pull request on GitLab.
